package com.boat;

import javax.annotation.Resource;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;

import com.boat.jdbc.BatchInsertRepository;

/**   
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author boat 
 * @date 2018年1月22日 下午3:10:58 
 * @version V1.0   
 */


@ComponentScan(basePackages = { "com.boat" })
public class Application implements CommandLineRunner{
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Resource
	private BatchInsertRepository batchInsertRepository;

	@Override
	public void run(String... args) throws Exception {
//		String[] sqls = new String[50000];
//		for (int i = 0; i < 50000; i++) {
//			String sql = "insert into t_user(name,age,day) value('" + "name" + i + "','" + i + "','" + "2018-01-19" + "')";
//			sqls[i] = sql;
//		}
		
		String[] sqls = new String[50000];
		for (int i = 0; i < 50000; i++) {
			String sql = "insert into student(stu_no,name,age,birthday,create_time) value('"+"201820"+i+"','name"+i+"','21','19970111','20180701')";
			sqls[i] = sql;
		}
		batchInsertRepository.batchExecute(sqls);
	}
}
